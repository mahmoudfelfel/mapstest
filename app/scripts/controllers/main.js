'use strict';

angular.module('projectsApp')
    .controller('MainCtrl', function($scope, $timeout, $document) {

        $scope.showDetails = false;
        $scope.noPlaces = true;

        var time = new Date();
        $scope.UTCTime = time.toUTCString();
        $scope.localTime = time.toLocaleString();

        if (localStorage.getItem("places")) {
            $scope.places = JSON.parse(localStorage["places"]);
            $scope.noPlaces = false;
        }

        $timeout(function() {
            var map = new GMaps({
                el: '#map',
                lat: -12.043333,
                lng: -77.028333
            });
            map.setContextMenu({
                control: 'map',
                options: [{
                    title: 'View Place Details',
                    name: 'view_details',
                    action: function(e) {
                        $scope.$apply(function() {
                            $scope.long = e.latLng.lng();
                            $scope.lat = e.latLng.lat();
                            $scope.detailsPosition = {
                                position: 'absolute',
                                left: e.pixel.x + 300,
                                top: e.pixel.y + 130
                            };
                            // TODO : make this service in another angular service module, inject it here as a debendency to use it, use promise and angular $http instead of jquery ajax 
                            $.ajax({
                                url: "https://maps.googleapis.com/maps/api/timezone/json?location=" + $scope.long + "," + $scope.lat + "&timestamp=" + (Math.round((new Date().getTime()) / 1000)).toString() + "&sensor=false",
                            }).done(function(response) {
                                if (response.timeZoneId != null) {
                                    $scope.$apply(function() {
                                        $scope.timeZone = response.timeZoneName;
                                    });
                                    var places = [];
                                    if (localStorage.getItem("places")) {
                                        places = JSON.parse(localStorage["places"]);
                                        if (places.indexOf($scope.timeZone) === -1) {
                                            places.push($scope.timeZone);
                                            localStorage["places"] = JSON.stringify(places);
                                        }
                                    } else {
                                        places.push($scope.timeZone);
                                        localStorage["places"] = JSON.stringify(places);

                                    }
                                }
                            });
                            $scope.showDetails = true;
                        })
                    }
                }]
            });

            $scope.removeDetails = function() {
                if ($scope.showDetails) {
                    $scope.showDetails = false;
                }
            }
        }, 1000);
    });
